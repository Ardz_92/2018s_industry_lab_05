package ictgradschool.industry.abstraction.farmmanager;

import ictgradschool.industry.abstraction.farmmanager.animals.IProductionAnimal;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane;

public class Chicken extends ictgradschool.industry.abstraction.farmmanager.animals.Animal implements IProductionAnimal {
    private int MAX_VALUE() {
        return 300;
    }

    public Chicken () {
        value = 200;
    }

    @Override
    public void feed() {
        if (value < MAX_VALUE()) {
            value = value + (MAX_VALUE() - 2) / 2;
        }
    }

    @Override
    public int costToFeed() {
        return 3;
    }

    @Override
    public String getType() {
        return "Chicken";
    }
    public String toString() {
        return getType() + " - $" + value;
    }

    @Override
    public boolean harvestable() {
        return true;
    }

    @Override
    public int harvest() {
        return 5;
    }
}
