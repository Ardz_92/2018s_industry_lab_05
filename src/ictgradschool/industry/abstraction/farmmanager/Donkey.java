package ictgradschool.industry.abstraction.farmmanager;

public class Donkey extends ictgradschool.industry.abstraction.farmmanager.animals.Animal {
    private int MAX_VALUE() {
        return 600;
    }

    public Donkey() {
        value = 500;
    }

    @Override
    public void feed() {
        if (value < MAX_VALUE()) {
            value = value + (MAX_VALUE() - 2) / 2;
        }
    }

    @Override
    public int costToFeed() {
        return 50;
    }

    @Override
    public String getType() {
        return "Donkey";
    }
    public String toString() {
        return getType() + " - $" + value;
    }
}
